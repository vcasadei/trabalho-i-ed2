#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 500

int MAX = 0;

/* Struct do arquivo books.dat - A Base de Dados */
typedef struct livro{
        char cod_livro[6];
        char titulo_livro_ptbr[51];
        char titulo_livro_original[51];
        char autor_livro[51];
        char ano_lancamento[5];
        char pais[26];
        int nota;
} Livro;

/* Struct do Indice Primario, contém o codigo do livro e o RRN */
typedef struct primaria{
        char chave_primaria[6];
        int RRN_livro;
} Primary;



/* Struct de Chave Secundário do Livro, contém o título do Livro e a Chave Primária (código do livro) */
typedef struct titulo{
        char chave_titulo[51];
        char chave_primaria[6];
} Title;

/* Struct de Chave Secundário do Livro, contém o autor do Livro e a Chave Primária (código do livro) */
typedef struct autor{
        char chave_autor[51];
        char chave_primaria[6];
} Author;

/*Ajusta Heap*/
void ajustaHeap_primary(Primary vet[], int i, int tam){

	int max;
	Primary aux;

	if (2*i+1 < tam && (strcmp(vet[2*i+1].chave_primaria, vet[i].chave_primaria) >= 0)) //verifica se o filho da esquerda é maior que a raiz
		max = 2*i+1;
	else
		max = i;
	if (2*i+2 < tam && (strcmp(vet[2*i+2].chave_primaria, vet[max].chave_primaria) >= 0)) //verifica se o filho da direita é o maior
		max = 2*i+2;
	/*se um dos filhos for o maior faz a troca*/
	if (max != i){
		aux = vet[max];
		vet[max] = vet[i];
		vet[i] = aux;
		ajustaHeap_primary(vet, max, tam); //ajusta o heap a partir da subarvore que tinha o maior
	}
}

/*Corpo do Heap*/
void heapSort_primary(Primary vet[], int tam){		
	
	int tamanho;
	Primary aux;

	for (tamanho = tam/2; tamanho >= 0; tamanho--)
		ajustaHeap_primary(vet, tamanho, tam);
	
	/*troca o primeiro elemento do vetor com o ultimo e ajusta o heap*/
	for (tamanho = tam - 1; tamanho >= 1; tamanho--){
		aux = vet[tamanho];
		vet[tamanho] = vet[0];
		vet[0] = aux;
		ajustaHeap_primary(vet, 0, tamanho);
	}	
}

/*Ajusta Heap*/
void ajustaHeap_title(Title vet[], int i, int tam){

	int max;
	Title aux;

	if (2*i+1 < tam && (strcmp(vet[2*i+1].chave_titulo, vet[i].chave_titulo) >= 0))
		max = 2*i+1;
	else
		max = i;
	if (2*i+2 < tam && (strcmp(vet[2*i+2].chave_titulo, vet[max].chave_titulo) >= 0))
		max = 2*i+2;
	/*se um dos títulos for maior alfabeticamente que o título no nó pai faz a troca*/
	if (max != i){
		aux = vet[max];
		vet[max] = vet[i];
		vet[i] = aux;
		ajustaHeap_title(vet, max, tam); //ajusta o heap a partir da subarvore que tinha o maior título
	}
}

/*Corpo do Heap*/
void heapSort_title(Title vet[], int tam){		
	
	int tamanho;
	Title aux;

	for (tamanho = tam/2; tamanho >= 0; tamanho--)
		ajustaHeap_title(vet, tamanho, tam);
	
	/*troca o primeiro elemento do vetor com o ultimo e ajusta o heap*/
	for (tamanho = tam - 1; tamanho >= 1; tamanho--){
		aux = vet[tamanho];
		vet[tamanho] = vet[0];
		vet[0] = aux;
		ajustaHeap_title(vet, 0, tamanho);
	}	
}

/*Ajusta Heap*/
void ajustaHeap_author(Author vet[], int i, int tam){

	int max;
	Author aux;

	if (2*i+1 < tam && (strcmp(vet[2*i+1].chave_autor, vet[i].chave_autor) >= 0))
		max = 2*i+1;
	else
		max = i;
	if (2*i+2 < tam && (strcmp(vet[2*i+2].chave_autor, vet[max].chave_autor) >= 0))
		max = 2*i+2;
	/*se um dos filhos for o maior faz a troca*/
	if (max != i){
		aux = vet[max];
		vet[max] = vet[i];
		vet[i] = aux;
		ajustaHeap_author(vet, max, tam); //ajusta o heap a partir da subarvore que tinha o maior
	}
}

/*Corpo do Heap*/
void heapSort_author(Author vet[], int tam){		
	
	int tamanho;
	Author aux;

	for (tamanho = tam/2; tamanho >= 0; tamanho--)
		ajustaHeap_author(vet, tamanho, tam);
	
	/*troca o primeiro elemento do vetor com o ultimo e ajusta o heap*/
	for (tamanho = tam - 1; tamanho >= 1; tamanho--){
		aux = vet[tamanho];
		vet[tamanho] = vet[0];
		vet[0] = aux;
		ajustaHeap_author(vet, 0, tamanho);
	}	
}

	/*Função que verifica se o código de um livro já existe*/
	int Verifica_codigo(Primary primarios[], char *cod, int ini, int fim){

		return 0;
	
	}

        /* Função que realiza a Inserção de um Novo Livro no Arquivo de Dados */
        void inserir_livro(Primary primario[], Author autor[], Title titulo[]){

                Livro registro;
                int n = 0, i;

                getchar();
                printf("Insira o titulo em portugues\n");
                scanf("%50[^\n]s", registro.titulo_livro_ptbr);
                getchar();
                
                registro.titulo_livro_original[0] = '+';
                registro.titulo_livro_original[1] = '-';
                printf("Insira o titulo Original\n");
                scanf("%50[^\n]s", registro.titulo_livro_original);
                getchar();
                
                if(strcmp(registro.titulo_livro_original, registro.titulo_livro_ptbr) == 0){
                        strcpy(registro.titulo_livro_original, "Idem");
                }
                
                if(registro.titulo_livro_original[0] == '+' && registro.titulo_livro_original[1] == '-'){
					strcpy(registro.titulo_livro_original, "Idem");
                }
					

			
                        
                printf("Insira o autor\n");
                scanf("%50[^\n]s", registro.autor_livro);
                getchar();
                
                printf("Insira o ano\n");
                scanf("%4[^\n]s", registro.ano_lancamento);
                getchar();
                
                printf("Insira o pais\n");
                scanf("%26[^\n]s", registro.pais);
                getchar();
                
                printf("Insira a nota\n");
                scanf("%d", &registro.nota);
                
                /* Geração da Chave Primária (Código do livro)*/
                for(i = 0; i < 3; i++){
                        registro.cod_livro[i] = toupper(registro.autor_livro[i]);
                }
                
                for(i = 3; i < 5; i++){
                        registro.cod_livro[i] = toupper(registro.ano_lancamento[i - 1]);
                }
                
                registro.cod_livro[5] = '\0';
                
                if(busca(primario, MAX, registro.cod_livro) == 1){
					return;
					
				} else {
					FILE *livros;

					if ((livros = fopen("books.dat", "a+")) == NULL)
						exit(-1);
					

					/*adiciona o novo registro nos indices*/
					strcpy (primario[MAX].chave_primaria, registro.cod_livro);
					primario[MAX].RRN_livro = MAX;
					strcpy (autor[MAX].chave_autor, registro.autor_livro);
					strcpy (autor[MAX].chave_primaria, registro.cod_livro);
					strcpy (titulo[MAX].chave_titulo, registro.titulo_livro_ptbr);
					strcpy (titulo[MAX].chave_primaria, registro.cod_livro);

					/*Ordena Indices*/
					heapSort_primary(primario, MAX+1);
					heapSort_title(titulo, MAX+1);
					heapSort_author(autor, MAX+1);

							/* Salvamento em Arquivo */
							fprintf(livros, "%s@%s@%s@%s@%s@%s@%d@", registro.cod_livro, registro.titulo_livro_ptbr, registro.titulo_livro_original, registro.autor_livro, registro.ano_lancamento, registro.pais, registro.nota);
						
							n += strlen(registro.ano_lancamento);
							n += strlen(registro.autor_livro);
							n += strlen(registro.cod_livro);
							n += strlen(registro.pais);
							n += strlen(registro.titulo_livro_original);
							n += strlen(registro.titulo_livro_ptbr);
							n += 1; //Para a nota
							n += 7; //Para os @
						
							/* Completando com o Caractere # */
							if(n < 192){
									for(i = 0; i < 192 - n; i++){
											fprintf(livros, "#");
									}
							}
					MAX++;

					fclose(livros);
				}
			
		
        }
       
/* Função que altera a nota de um Livro no Arquivo de Dados */
void alterar_nota(Primary p[]){
	char s[6], c;
        int RRN, nota, cont, i;
	FILE *livros;

	if ((livros = fopen("books.dat", "r+")) != NULL){

        	RRN = -1;
                cont = 0;
                scanf("%s", s);

		for(i = 0; i < MAX; i++){
			if(strcmp(p[i].chave_primaria, s) == 0){
				RRN = p[i].RRN_livro;
				break;
			}
		}

                if(RRN == -1){
			printf("Registro não encontrado!\n");
			return;
		} else {
			printf("Informe a nota\n\n");
			scanf("%d", &nota);

			if(nota >= 0 && nota <= 9){
				fseek(livros, RRN * 192, SEEK_SET);
				for(i = 0; cont <= 7; i++){
					c = fgetc(livros);
					if(c == '@'){
						cont++;
					}	
					if(cont == 6){
						break;
					}	
				}
				fprintf(livros, "%d", nota);
			} else {
				printf("Nota inválida!\n");
			}
		}
				
		fclose(livros);
	} else {
		printf("Falha ao abrir arquivo de dados\n");
	}
}

/*Função que remove um indíce de titulo de um registro que foi removido no arquivo de dados*/
void remover_indice_titulo(Title titulo[], char *s){

	int i;

	for (i = 0; i < MAX; i++){
		if (strcmp(titulo[i].chave_primaria, s) == 0){
			titulo[i].chave_primaria[0] = '*';
			titulo[i].chave_primaria[1] = '|';
			break;
		}
	}
}

/*Função que remove um indíce de autor de um registro que foi removido no arquivo de dados*/
void remover_indice_autor(Author autor[], char *s){

	int i;

	for (i = 0; i < MAX; i++){
		if (strcmp(autor[i].chave_primaria, s) == 0){
			autor[i].chave_primaria[0] = '*';
			autor[i].chave_primaria[1] = '|';
			break;
		}
	}
}

/* Função que realiza a Remoção de um Livro do Arquivo de Dados */
void remover_livro(Primary p[], Title t[], Author a[]){
	char s[6];
	int i;
	FILE *livros;

	if ((livros = fopen("books.dat", "r+")) != NULL){

		scanf("%s", s);

		for(i = 0; i < MAX; i++){
			if(strcmp(p[i].chave_primaria, s) == 0){
				fseek(livros, p[i].RRN_livro * 192, SEEK_SET);
				fprintf(livros, "*|");
				
				/*Coloca como removido no índice primário*/
				p[i].chave_primaria[0] = '*';
				p[i].chave_primaria[1] = '|';
				
				/*Coloca como removido nos indíces secundários*/
				remover_indice_titulo(t, s);
				remover_indice_autor(a, s);
				fclose(livros);
				return;
			}
		}
					
		printf("Registro não encontrado!\n");
		fclose(livros);
	}
}

/*Função que exibe um registro*/
void exibir_registro(char *registro){

	int i, j;

	i = j = 0;
	/*Exibe a chave primária*/
	while (j < 5){
		printf("%c", registro[i]);
		i++;
		j++;
	}
	printf("\n");
	i++;
	/*exibe o titulo em português*/
	while (registro[i] != '@'){
		printf("%c", registro[i]);
		i++;
	}
	printf("\n");
	i++;
	/*exibe o título original, se for o mesmo que em português exibe 'idem'*/
	while (registro[i] != '@'){
		printf("%c", registro[i]);
		i++;
	}
	printf("\n");
	i++;
	/*Exibe o autor*/
	while (registro[i] != '@'){
		printf("%c", registro[i]);
		i++;
	}
	printf("\n");
	i++;
	/*Exibe o ano*/
	while (registro[i] != '@'){
		printf("%c", registro[i]);
		i++;
	}
	printf("\n");
	i++;
	/*Exibe o país*/
	while (registro[i] != '@'){
		printf("%c", registro[i]);
		i++;
	}
	printf("\n");
	i++;
	/*Exibe a nota*/
	printf("%c\n", registro[i]);
	return;
}
       
/* Função que faz a Busca Binária de um Livro por Código, a chave primária tem que ser informada completa */
int busca(Primary primarios[], int tam, char *chave){

	int ini = 0, fim = tam-1, meio, x;
	char registro[193];
	FILE *livros;

	if ((livros = fopen("books.dat", "r")) != NULL){

		while (ini <= fim){
			meio = (ini+fim)/2;
			
			/*Converte a chave informa para a forma canônica*/
			x = 0;
			while (chave[x] != '\0'){
				chave[x] = toupper(chave[x]);
				x++;
			}
	
			x = strcmp(primarios[meio].chave_primaria, chave); //verifica se a chave é menor ou maior do que o elemento do meio
			if (x == 0){
				return 1;
			} else if (x < 0) {
				ini = meio + 1;
			} else {
				fim = meio - 1;
			}
		}
		return -1;

	} else {
		exit(-1);
	}
}

 
/* Função que faz a Busca Binária de um Livro por Código, a chave primária tem que ser informada completa */
void busca_codigo(Primary primarios[], int tam, char *chave){

	int ini = 0, fim = tam-1, meio, x;
	char registro[193];
	FILE *livros;

	if ((livros = fopen("books.dat", "r")) != NULL){

		while (ini <= fim){
			meio = (ini+fim)/2;
			
			/*Converte a chave informa para a forma canônica*/
			x = 0;
			while (chave[x] != '\0'){
				chave[x] = toupper(chave[x]);
				x++;
			}
	
			x = strcmp(primarios[meio].chave_primaria, chave); //verifica se a chave é menor ou maior do que o elemento do meio
			if (x == 0){
				fseek(livros, primarios[meio].RRN_livro * 192, SEEK_SET);
				fgets(registro, 193, livros); //lê todo o registro
				exibir_registro(registro);
				return;
			} else if (x < 0) {
				ini = meio + 1;
			} else {
				fim = meio - 1;
			}
		}
		printf("Registro não encontrado!\n");

	} else {
		printf("Erro ao abrir o arquivo de dados\n");
		exit(-1);
	}
}
        
        /* Função que faz a Busca de um Livro por Título em Português */
        void busca_titulo(Primary primarios[], Title titulos[], int tam){
	
		int meio, ini = 0, fim = tam - 1, x, qntd;
		char titulo[51];

		getchar();
		scanf("%[^\n]", titulo);
		qntd = strlen(titulo);


		while (ini <= fim){
			
			meio = (ini+fim)/2;

			/*Converte a chave informa para a forma canônica*/
			x = 0;
			while (titulo[x] != '\0'){
				titulo[x] = toupper(titulo[x]);
				x++;
			}

			x = strncmp(titulos[meio].chave_titulo, titulo, qntd);
			if (x == 0){
				busca_codigo(primarios, MAX, titulos[meio].chave_primaria);
				return;
			} else if (x < 0)
				ini = meio + 1;
			else
				fim = meio - 1;

		}
        }
        
        /* Função que faz a Busca de um Livro por Autor */
        void busca_autor(Primary primarios[], Author autores[], int tam){

		int meio, ini = 0, fim = tam, x, qntd;
		char autor[51];

		getchar();
		scanf("%51[^\n]s", autor);
		qntd = strlen(autor);

			while (ini <= fim){
				
				meio = (ini+fim)/2;

				/*Converte a chave informa para a forma canônica*/
				x = 0;
				while (autor[x] != '\0'){
					autor[x] = toupper(autor[x]);
					x++;
				}

				x = strncmp(autores[meio].chave_autor, autor, qntd);
				if (x == 0){
					busca_codigo(primarios, MAX, autores[meio].chave_primaria);
					return;
				} else if (x < 0)
					ini = meio + 1;
				else
					fim = meio - 1;
			}
                
        }
    
/* Função que Lista todos os Livros de Forma Formatada, tomando o cuidado para não mostrar os livros deletados */
void listar_livros(Primary p[]){
			
	char s[51], c, books[193];
	int i, j = 0;
	FILE *livros;

	if ((livros = fopen("books.dat", "r")) == NULL || MAX == 0){

		printf("Arquivo vazio!\n");
		return;
	} else {
		while(j < MAX){
			if(p[j].chave_primaria[0] != '*' && p[j].chave_primaria[1] != '|'){
				fseek(livros, p[j].RRN_livro * 192, SEEK_SET);
				fgets(books, 193, livros);
				c = fgetc(livros);
				exibir_registro(books);
				printf("\n");
			}
			j++;
		}
	}	
}
        
/*Função que salva o índice primário no arquivo*/
void salvar_indice_primario(Primary p[], int flag){
			
	int i;
	FILE *iprimaria;

	iprimaria = fopen("iprimary.idx","w");
        fprintf(iprimaria, "%d\n", flag);

	for(i = 0; i < MAX; i++){
		if(p[i].chave_primaria[0] != '*' && p[i].chave_primaria[1] != '|'){
			fprintf(iprimaria, "%s ", p[i].chave_primaria);
			fprintf(iprimaria, "%d\n", p[i].RRN_livro);
		}
	} 

	fclose(iprimaria);
}
        
/*Função que salva os dados do índice de títulos no arquivo de indíce de título*/
void salvar_indice_titulo(Title titulos[], int flag){

	FILE *titulo;
	int i, x, j;

	if ((titulo = fopen("ititle.idx", "w")) != NULL){
		fprintf(titulo, "%d\n", flag);
		for (i = 0; i < MAX; i++)
			if (titulos[i].chave_primaria[0] != '*' && titulos[i].chave_primaria[1] != '|'){
				
				/*Coloca a chave na sua forma canônica*/
				j = 0;
				while (titulos[i].chave_titulo[j] != '\0'){
					titulos[i].chave_titulo[j] = toupper(titulos[i].chave_titulo[j]);
					j++;
				}

				if ((x = strlen(titulos[i].chave_titulo)) == 50)
					fprintf(titulo, "%s %s\n", titulos[i].chave_titulo, titulos[i].chave_primaria);
				else {
					fprintf(titulo, "%s", titulos[i].chave_titulo);
					for (j = 0; j < (50-x); j++)
						fprintf(titulo, " ");
					fprintf(titulo, "%s\n", titulos[i].chave_primaria);
				}
			}
		fclose(titulo);
	}
}

/*Função que salva os dados do índice de títulos no arquivo de indíce de título*/
void salvar_indice_autor(Author autores[], int flag){

	FILE *autor;
	int i, x, j;

	if ((autor = fopen("iauthor.idx", "w")) != NULL){
		fprintf(autor, "%d\n", flag);
		for (i = 0; i < MAX; i++){
			if (autores[i].chave_primaria[0] != '*' && autores[i].chave_primaria[1] != '|'){
				
				/*Coloca a chave na sua forma canônica*/
				j = 0;
				while (autores[i].chave_autor[j] != '\0'){
					autores[i].chave_autor[j] = toupper(autores[i].chave_autor[j]);
					j++;
				}

				if ((x = strlen(autores[i].chave_autor)) == 50)
					fprintf(autor, "%s %s\n", autores[i].chave_autor, autores[i].chave_primaria);
				else {
					fprintf(autor, "%s", autores[i].chave_autor);
					for (j = 0; j < (50-x); j++)
						fprintf(autor, " ");
					fprintf(autor, "%s\n", autores[i].chave_primaria);
				}
			}
		}
		fclose(autor);
	}
}

/* Função que finaliza o Programa, aqui são salvos os arquivos de Índice e é setada a Flag de atualizado no cabeçalho deles */
void finalizar_programa(Primary primarios[], Title titulos[], Author autores[]){
	salvar_indice_primario(primarios, 1);
	salvar_indice_titulo(titulos, 1);
	salvar_indice_autor(autores, 1);
}
        
        
/* Função que Lê o Indice Primário para a estrutura */
void ler_indice(FILE *iprimaria, Primary p[]){

	char s[6];

        while(!feof(iprimaria)){				
		fscanf(iprimaria, "%5s", s);
		if(!feof(iprimaria)){
			strcpy(p[MAX].chave_primaria, s);
			fscanf(iprimaria, "%d", &p[MAX].RRN_livro);
			MAX++;
		}
	}		

	/*seta a flag com 0*/
	fseek(iprimaria, 0, SEEK_SET);
	fprintf(iprimaria, "0");
	fclose(iprimaria);
}
        
/* Função que Lê o Índice Secundário de Título para a estrutura */
void ler_titulo(FILE * ititulo, Title titulos[]){
		
	char c, aux[56];
	int i = 0, j, ultimo;

        while (!feof(ititulo)){

		fgetc(ititulo);
		fgets(aux, 56, ititulo);
		aux[55] = '\0';

		for (j = 50; j < 55; j++)
			titulos[i].chave_primaria[j-50] = aux[j];
	
		ultimo = 0;
		for (j = 0; j < 49; j++)
			if (aux[j] >= 'A' && aux[j] <= 'Z')
				ultimo = j;

		for (j = 0; j <= ultimo; j++)
			titulos[i].chave_titulo[j] = aux[j];

		i++;
	}

	/*Seta a flag do índice como 0*/		
	fseek(ititulo, 0, SEEK_SET);
	fprintf(ititulo, "0");
	fclose(ititulo);
}
        
/* Função que Lê o Índice Secundário de Autor para a estrutura */
void ler_autor(FILE * iautor, Author autores[]){
		
	char c, aux[56];
	int i = 0, j, ultimo;

        while (!feof(iautor)){

		fgetc(iautor);
		fgets(aux, 56, iautor);
		aux[55] = '\0';

		for (j = 50; j < 55; j++)
			autores[i].chave_primaria[j-50] = aux[j];
	
		ultimo = 0;
		for (j = 0; j < 49; j++)
			if (aux[j] >= 'A' && aux[j] <= 'Z')
				ultimo = j;

		for (j = 0; j <= ultimo; j++)
			autores[i].chave_autor[j] = aux[j];

		i++;
	}

	/*Seta a flag do índice como 0*/
	fseek(iautor, 0, SEEK_SET);
	fprintf(iautor, "0");
	fclose(iautor);
}
        
/* Função que insere um novo elemento de forma ordenada na Struct de Chave Primária */
void inserir_primario(FILE *livros, Primary primarios[], int i){

	char aux2[6];
                
	fseek(livros, i * 192, SEEK_SET);
	
	fscanf(livros, "%5s", aux2);
	
	if(aux2[0] != '*' && aux2[1] != '|'){
		strcpy(primarios[MAX].chave_primaria, aux2);
		primarios[MAX].RRN_livro = i;
		MAX++;
	}        
}
        
/* Função que Cria o Índice Primário, caso este não exista ou esteja desatualizado */
void criar_primario(FILE *livros, Primary primarios[]){

	int i, n;  
                
	fseek(livros, 0, SEEK_END);
	
	n = ftell(livros);
	
	fseek(livros, 0, 0);
	
	n = n/192;
	
	//Chamada de Função para inserir os registros
	for(i = 0; i < n; i++){
			inserir_primario(livros, primarios, i);
	}

	heapSort_primary(primarios, MAX);
        salvar_indice_primario(primarios, 0);
}

/* Função que Cria o Índice Secundário de Título, caso este não exista ou esteja desatualizado */
void criar_titulo(FILE *livros, Title titulos[], Primary primarios[]){

	int i = 0;

	while (i < MAX){
		strcpy(titulos[i].chave_primaria, primarios[i].chave_primaria);
		fseek(livros, 192 * primarios[i].RRN_livro + 6, SEEK_SET);
		fscanf(livros, "%50[^@]s", titulos[i].chave_titulo);
		i++;
	}

	heapSort_title(titulos, MAX);
	salvar_indice_titulo(titulos, 0);
}
        
/* Função que Cria o Índice Secundário de Autor, caso este não exista ou esteja desatualizado */
void criar_autor(FILE *livros, Author autores[], Primary primarios[]){
                
	int i = 0, cont;
	char aux[51], c;

	while (i < MAX){
		cont = 2;
		strcpy(autores[i].chave_primaria, primarios[i].chave_primaria);
		fseek(livros, 192 * primarios[i].RRN_livro + 6, SEEK_SET);
		while (cont){
			fscanf(livros, "%50[^@]s", aux);
			cont--;
			fscanf(livros, "%c", &c);
		}
		fscanf(livros, "%50[^@]s", autores[i].chave_autor);
		i++;
	}

	heapSort_author(autores, MAX);
	salvar_indice_autor(autores, 0);
}
        
        /* Função que libera o espaço alocado dos registros */
        void liberar_espaco(FILE * livros, FILE * iprimaria, FILE * ititulo, FILE * iautor, Primary p[]){
			FILE * aux;		
			livros = fopen("books.dat","r+");
			char c;
			aux = fopen(".temp", "w");
			fseek(livros, 0, SEEK_SET);
			while(!feof(livros)){
				fscanf(livros, "%c", &c);
				if(c == '*'){
					fscanf(livros, "%c", &c);
					if(c == '|'){
						fseek(livros, 190, SEEK_CUR);
					} else{
						fprintf(aux, "*%c", c);
					}
				} else {
					if(!feof(livros))
						fprintf(aux, "%c", c);
					}
				}
			fclose(aux);
			fclose(livros);
			remove("books.dat");
			rename(".temp", "books.dat");
        }

/*Função que carrega todos os arquivos de índice*/
void carregar_arquivos_indices(FILE *livros, Primary primario[], Title titulo[], Author autor[]){

	FILE *arq;
	int flag;

	/*verifica se índice primario existe e se está atualizado*/
	if ((arq = fopen("iprimary.idx", "r+")) != NULL){
		fscanf(arq, "%d", &flag);
		if (flag == 1)
			ler_indice(arq, primario);
		else 
			criar_primario(livros, primario);
	} else 
		criar_primario(livros, primario);

	/*verifica se índice secundário de autor existe e se está atualizado*/
	if ((arq = fopen("ititle.idx", "r+")) != NULL){
		fscanf(arq, "%d", &flag);
		if (flag == 1){
			ler_titulo(arq, titulo);
		} else
			criar_titulo(livros, titulo, primario);
		
	} else
		criar_titulo(livros, titulo, primario);

	/*verifica se índice secundário de título existe e se está atualizado*/
	if ((arq = fopen("iauthor.idx", "r+")) != NULL){
		fscanf(arq, "%d", &flag);
		if (flag == 1){
			ler_autor(arq, autor);
		}else
			criar_autor(livros, autor, primario);
		
	} else 
		criar_autor(livros, autor, primario);

	fclose(livros);
}
 
/*FUNÇÃO PRINCIPAL*/              
int main(){
        int selecao = -1;
		char chave[6];
        FILE *livros, *iprimaria, *ititulo, *iautor;
        Primary primarios[N];
        Title titulos[N];
        Author autores[N];
        
        /* Abrindo Arquivo de Dados */
        if((livros = fopen("books.dat","r+")) != NULL){
                carregar_arquivos_indices(livros, primarios, titulos, autores);
        } else {
		livros = fopen("books.dat", "w+");
		if (livros != NULL)
			carregar_arquivos_indices(livros, primarios, titulos, autores);
	}
        
        while(selecao != 7){
                scanf("%d", &selecao);
                switch (selecao){
                        /* Cadastro: O usuário deve inserir um novo livro */
                        case 1:
                                inserir_livro(primarios, autores, titulos);
                                break;
                        /* Alteração: O usuário deve ser capaz de alterar a NOTA de um livro, informando a chave primária */
                        case 2:
                                alterar_nota(primarios);
                                break;
                        /* Remoção: O usuário deve ser capaz de remover um livro */
                        case 3:
                                remover_livro(primarios, titulos, autores);
                                break;
                        /* Busca, o usuário deverá ser capaz de buscar um livro */
                        
                        /* Por código: O usuário deve informar a chave primária */
                        case 41: 
								scanf("%5s", chave);
                                busca_codigo(primarios, MAX, chave);
                                break;
                        /* Por título: É solicitado o título em português */
                        case 42:
                                busca_titulo(primarios, titulos, MAX);
                                break;
                        /* Por Autor: É solicidado o sobrenome-nome do autor */
                        case 43:
                                busca_autor(primarios, autores, MAX);
                                break;
                        
                        /* Listagem: Listar de forma formatada todos os registros */
                        case 5:
                                listar_livros(primarios);
                                break;
                        /* Liberar Espaço: O Arquivo de dados deve ser reorganizado, rmovendo livros */
                        case 6:
                                liberar_espaco(livros, iprimaria, ititulo, iautor, &(*primarios));
                                break;
                        /* Finalizar: Encerra o programa e atualiza indices */
                        case 7:
                                finalizar_programa(primarios, titulos, autores);
                                selecao = 7;
                                break;
			default:
				printf("Opção Inválida\n"); break;
                }       
        }

        return 0;
}
